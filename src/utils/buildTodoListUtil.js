const buildTodoListItem = (itemContent) => {
    return {
        content: itemContent,
        done: false,
        description: 'default decription'
    }
}
export {
    buildTodoListItem
}