const flag = 1;
const promise = new Promise((resolve, reject)=>{
    if(flag === 0) {
        reject(flag)
    } else {
        resolve(flag);
    }
})

promise.then((resolvedValue)=>{
    console.log(resolvedValue);
}).catch((rejectedValue)=> {
    console.log(rejectedValue);
})