import { createSlice } from '@reduxjs/toolkit'

export const todoListSlice = createSlice({
    name: 'todoList',
    initialState: {
        todoListItems: []
    },
    reducers: {
        initTodos: (state, action) => {
            state.todoListItems = action?.payload;
        },
        clearTodoListItems: (state, action) => {
            state.todoListItems = []
        }
    }
})

export const { clearTodoListItems, initTodos } = todoListSlice.actions

export default todoListSlice.reducer