import { loadTodos } from '../apis/todo';
import { initTodos } from '../store/todoListSlice';
import { useDispatch } from 'react-redux';
import { useRef } from 'react';

export const useTodo = () => {

    const dispatch = useDispatch()
    const reloadTodos = () => {
        loadTodos().then((data) => {
            dispatch(initTodos(data))
        })
    }
    return useRef({ reloadTodos }).current
}