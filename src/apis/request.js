import axios from 'axios';

const request = axios.create({
    // baseURL: 'https://64c0c5620d8e251fd11288d6.mockapi.io/',
    baseURL: 'http://localhost:8081/',
    timeout: 60000,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
});

request.interceptors.response.use(response => {
    return response.data.data;
}, error => {
    const data = {
        msg: error?.response?.data?.msg || '发生未知错误',
        fail: true,
    }
    return data;
})


export default request;