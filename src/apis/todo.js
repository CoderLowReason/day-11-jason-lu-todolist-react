import request from "./request";

export const loadTodos = () => {
    return request.get('/todos')
}

export const toggleTodo = ({ id, item }) => {
    return request.put(`/todos/${id}`, {
        ...item,
        id
    })
}

export const createTodo = (todo) => {
    return request.post(`/todos`, todo)
}

export const deleteTodo = (id) => {
    return request.delete(`/todos/${id}`)
}

export const updateTodo = (id, item) => {
    return request.put(`/todos/${id}`, item)
}

export const getDoneTodoItemById = (id) => {
    return request.get(`/todos/${id}`)
}
