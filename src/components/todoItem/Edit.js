import React, { useState } from 'react'
import './TodoItem.css';
import { Button, Modal, Input } from 'antd';

export default function Edit(props) {
    const { closeModifyModal, item, isOpenModal, confirmModify } = props;
    const [editContent, setEditContent] = useState(item?.content);

    const onChangeHandler = ({ target: { value } }) => {
        setEditContent(value?.trim())
    }
    const onConfirmModify = () => {
        confirmModify({ editContent, id: item.id });
    }
    return (
        <Modal
            open={isOpenModal}
            title="编辑"
            onCancel={closeModifyModal}
            footer={[
                <Button key="cancel" onClick={closeModifyModal}>取消</Button>,
                <Button key="submit" type="primary" onClick={onConfirmModify}>提交</Button>
            ]}>
            <Input value={editContent} onChange={onChangeHandler} placeholder="Please type your todo item here." />
        </Modal>
    )
}
