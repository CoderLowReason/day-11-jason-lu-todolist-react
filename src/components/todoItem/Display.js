import React from 'react'
import './TodoItem.css';
import { useNavigate } from 'react-router-dom';
import { toggleTodo } from '../../apis/todo';
import { useTodo } from '../../hooks/useTodo';
import { Button, Popconfirm } from 'antd';

export default function Display(props) {
    const { item, onDelete, isDonePage, showModifyModal } = props;
    const { done, id } = item;
    const navigate = useNavigate();
    const { reloadTodos } = useTodo();

    const toggleDone = async () => {
        if (isDonePage) {
            navigate(`/todo/${id}`);
        } else {
            console.log(item);
            await toggleTodo({ id, item: { ...item, done: !done } });
            reloadTodos();
        }
    }
    return (
        <div className='item'>
            <span className={done ? 'delete itemContent' : 'itemContent'} onClick={toggleDone}> {item.content} </span>
            <div className='editButtons' style={isDonePage ? { 'display': 'none' } : {}}>
                <Button type='primary' onClick={showModifyModal}>Modify</Button>
                <Popconfirm
                    title="Delete the todo item"
                    description="Are you sure to delete this item?"
                    okText="Yes"
                    cancelText="No"
                    onConfirm={() => onDelete(id)}
                >
                    <Button danger>Delete</Button>
                </Popconfirm>
            </div>
        </div>
    )
}
