import React, { useState } from 'react'
import Display from './Display';
import Edit from './Edit';
import { buildTodoListItem } from "../../utils/buildTodoListUtil";
import { deleteTodo, updateTodo } from '../../apis/todo';
import { useTodo } from '../../hooks/useTodo';
import { message } from 'antd';

export default function TodoItem(props) {
  const { index, item, isDonePage } = props;
  const { reloadTodos } = useTodo();
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [messageApi, contextHolder] = message.useMessage();

  const error = (errorMsg) => {
    messageApi.open({
      type: 'error',
      content: errorMsg,
    });
  };

  const success = (successMsg) => {
    messageApi.open({
      type: 'success',
      content: successMsg,
    });
  };

  const showModifyModal = () => {
    setIsOpenModal(true);
  };

  const closeModifyModal = () => {
    setIsOpenModal(false);
  };

  const onDelete = async (id) => {
    await deleteTodo(id);
    reloadTodos();
  }

  const confirmModify = async ({ id, editContent }) => {
    const newItem = buildTodoListItem(editContent?.trim());
    const data = await updateTodo(id, newItem);
    if (data.fail) {
      error(data.msg || '修改失败');
    } else {
      success("修改成功")
      reloadTodos();
      setIsOpenModal(false);
    }
  }

  return (
    <>
      {contextHolder}
      <Edit item={item} index={index} closeModifyModal={closeModifyModal} confirmModify={confirmModify} isOpenModal={isOpenModal} />
      <Display onDelete={onDelete} index={index} item={item} isDonePage={isDonePage} showModifyModal={showModifyModal} />
    </>
  )
}
