import React, { useState } from 'react';
import './TodoGenerator.css';
import { useDispatch } from "react-redux";
import { clearTodoListItems } from '../../store/todoListSlice';
import { buildTodoListItem } from "../../utils/buildTodoListUtil";
import { createTodo } from '../../apis/todo';
import { Button, Input } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { message } from 'antd';
import { useTodo } from '../../hooks/useTodo';

const ENTER_KEY_DOWN = 13;
export default function TodoGenerator() {
  let [itemContent, setItemContent] = useState('');
  const dispatch = useDispatch();
  const [messageApi, contextHolder] = message.useMessage();
  const { reloadTodos } = useTodo();
  const error = (errorMsg) => {
    messageApi.open({
      type: 'error',
      content: errorMsg,
    });
  };

  const success = (successMsg) => {
    messageApi.open({
      type: 'success',
      content: successMsg,
    });
  };

  const onChangeHandler = ({ target: { value } }) => {
    setItemContent(value);
  }
  const addItem = async () => {
    const item = buildTodoListItem(itemContent);
    const data = await createTodo(item);
    if (data.fail) {
      error('添加失败');
    } else {
      reloadTodos();
      success('添加成功');
    }
    setItemContent('');
  }
  const deleteAllItems = () => {
    dispatch(clearTodoListItems());
  }
  const onKeyDownHanlder = ({ keyCode }) => {
    if (keyCode === ENTER_KEY_DOWN) {
      addItem();
    }
  }
  return (
    <>
      {contextHolder}
      <div className='generator'>
        <Input className='inputTodo' onChange={onChangeHandler} onKeyDown={onKeyDownHanlder} size="large" placeholder="please type your todo here" value={itemContent} prefix={<UserOutlined />} />
        <Button className='add' type="primary" onClick={addItem}>add</Button>
        <Button className='deleteAll' type="primary" danger onClick={deleteAllItems}>deleteAll</Button>
      </div>
    </>
  )
}
