import React from 'react'
import TodoItem from '../todoItem/TodoItem';

export default function TodoGroup(props) {
  const { items, isDonePage } = props;
  return (
    <div>
      {items?.map((item, index) => <TodoItem index={index} key={item.id} item={item} isDonePage={isDonePage} />)}
    </div>
  )
}
