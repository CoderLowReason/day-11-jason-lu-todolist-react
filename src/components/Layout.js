import React from 'react';
import { NavLink, Outlet } from 'react-router-dom';
export default function Layout() {
    return (
        <div style={{ display: 'flex', alignItems: 'center', flexFlow: 'column' }}>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <NavLink to='/'>TodoList</NavLink>
                ---------------------
                <NavLink to='/done'>DoneList</NavLink>
            </div>
            <Outlet></Outlet>
        </div>
    )
}
