import React, { useEffect } from 'react'
import TodoGenerator from '../todoGenerator/TodoGenerator'
import './TodoList.css';
import TodoGroup from '../todoGroup/TodoGroup';
import { useSelector } from "react-redux";
import { useTodo } from '../../hooks/useTodo';

export default function TodoList() {
  const { reloadTodos } = useTodo();
  useEffect(() => {
    reloadTodos();
  }, [reloadTodos])
  const items = useSelector(state => state.todoList.todoListItems);
  return (
    <div className='content'>
      <div className='banner'>TodoList</div>
      <TodoGroup items={items} />
      <TodoGenerator />
    </div>
  )
}
