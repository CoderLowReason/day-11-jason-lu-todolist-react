import React from 'react'
import './DoneList.css';
import TodoGroup from '../todoGroup/TodoGroup';
import { useSelector } from "react-redux";

export default function TodoList(props) {
    const { isDonePage } = props;
    const items = useSelector(state => state.todoList.todoListItems);
    const doneItems = items?.filter((item) => item.done);
    return (
        <div className='content'>
            <div className='banner'>DoneList</div>
            <TodoGroup isDonePage={isDonePage} items={doneItems} />
        </div>
    )
}
