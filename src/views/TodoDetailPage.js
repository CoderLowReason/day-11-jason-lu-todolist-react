import React, { useEffect, useState } from 'react'
import { getDoneTodoItemById } from '../apis/todo';
import { useParams } from 'react-router-dom'
import { Card } from 'antd'
export default function TodoDetailPage() {
    const [item, setItem] = useState({})
    const [loading, setLoading] = useState(true)

    const { id } = useParams();
    useEffect(() => {
        getDoneTodoItemById(id).then((data) => {
            setItem(data);
            setLoading(false);
        });
    }, [id, loading])
    return (
        <Card loading={loading} title="Done List Items" bordered={false} style={{ width: 300 }}>
            <p>id---------{item?.id}</p>
            <p>content---------{item?.content}</p>
            <p>done---------{`${item?.done}`}</p>
        </Card>
    )
}
