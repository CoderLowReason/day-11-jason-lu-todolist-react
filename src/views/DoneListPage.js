import DoneList from '../components/todoList/DoneList.js';
import React from "react";
function DoneListPage() {
    return <DoneList isDonePage={true} />
}

export default DoneListPage;
