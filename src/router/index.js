import * as React from "react";
import {
    createBrowserRouter,
} from "react-router-dom";
import TodoListPage from "../views/TodoListPage";
import DoneListPage from "../views/DoneListPage";
import TodoDetailPage from "../views/TodoDetailPage";
import NotFoundPage from "../views/NotFoundPage";
import Layout from '../components/Layout'
const router = createBrowserRouter([
    {
        path: '/',
        element: <Layout />,
        children: [
            {
                path: "/",
                element: (<TodoListPage></TodoListPage>),
            },
            {
                path: "/done",
                element: <DoneListPage></DoneListPage>,
            },
            {
                path: "/todo/:id",
                element: <TodoDetailPage></TodoDetailPage>,
            },
        ]
    },
    {
        path: '*',
        element: <NotFoundPage></NotFoundPage>
    }
]);


export default router;