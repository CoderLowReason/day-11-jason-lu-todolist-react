### 1.Objectives

1. During code review I recalled that empty&null&undefined check for user input.
2. I finished all the function on "todoList" homework and refactored my code which made me feel proud.
3. Learned redux&react-redux&react-redux-toolkit.I learned it before, so I did not have much trouble learning them.
4. I learned the unit test on react.
5. I did a lot of search about my part of preparation of "User Story Session" and have done it, our team will practice for the presentation tomorrow.


### 2.Reflective

​	 I felt gainful and challenging on today's react&redux learning.And I will keep on learning more.

### 3. Interpretive

​	the most impressive thing today is 1. I finished the whole "todoList" demo, and i refactor the code using redux which helped me strengthen the grasp of it.

### 4. Decisional

    keep learning on react&redux&react-redux&react-redux-toolkit and the unit test in frontend including function test and UI test
