### 1.Objectives

1. I shared my information with the team in this morning's code review. The use of the operator in es6 has also found some problems with its own code.
2. Exchanged opinions with team members and teachers, and modified the PPT content of the Session.
3. Learned react-router knowledge and added routes to existing codes.
4. The usage of various hook functions in the route useParam, useNavigate, useSearchParams, etc., and the unified error page processing jumps to 404.
5. Learned how to use axios and react to realize data interaction between front and back end.
6. Reviewed the use of antd framework to optimize react interface, in fact, the essence is the same as ElementUI.


### 2.Reflective

​	 I felt gainful and challenging on today's react&axios&foxapi learning.And I will keep on learning more.

### 3. Interpretive

​	We finished the frontend learning today,and I done it not bad. I grasped the react&redux&axios&antd, but learning is a combination of listening and practice, and I know there is still long way to go.

### 4. Decisional

    keep learning on react&redux&react-redux&react-redux-toolkit&antd and the unit test in frontend including function test and UI test
